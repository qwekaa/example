<?php

namespace app\modules\analitica\models\permission;

use app\modules\analitica\models\report\FilterForm;
use app\modules\analitica\models\forms\FilterFormFactory as FormFactory;

/**
 * Инкапсулирует пользовательский фильтр с фильтром, который определяется,
 * исходя из роли пользователя. Так же этот класс генерирует экземпляр FilterForm
 */
class GainPermission implements IPermission
{
    private $UserPermission;
    
    /**
     * Данные фильтра из запроса
     * @var array
     */
    private $filter;
    
    /**
     * @var FilterForm
     */
    private $FilterForm;
    
    private $formType;
    
    public function __construct(\IUserPermission $UserPermission, array $filter, $formType = FormFactory::CURRDAY)
    {
        $this->setUserPermission($UserPermission);
        $this->setFilter($filter);
        $this->formType = $formType;
    }
    
    /**
     * Получить экземпляр класса фильтра с учетом разрешений
     * @return type
     */
    public function getFilterForm()
    {
        if (empty($this->FilterForm)) {
            $this->FilterForm = FormFactory::instance($this->formType);
            $this->FilterForm->attributes = $this->getFilterWithPermission();
            $this->FilterForm->validate();
        }
        return $this->FilterForm;
    }
    
    /**
     * Получить польный пользовательский фильтр с учетом фильтра из формы
     * и разрешений в зависимости от роли
     * @return array
     */
    public function getFilterWithPermission()
    {
        $permission = $this->getPermissionByUser();
        return array_merge($this->getFilter(), $permission);
    }
    
    /**
     * Определелить фильтр, исходя из роли пользователя
     * @return type
     */
    public function getPermissionByUser()
    {
        $UserPermission = $this->getUserPermission();
        $role = $UserPermission->getRole();
        if ($role == 'manager') { //показываем только группу менеждера
            return [
                'city' => $UserPermission->getCityId(),
                'group' => $UserPermission->getGroupId(),
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'rgp') { //показываем только город РГП
            return [
                'city' => $UserPermission->getCityId(),
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'director' || $role == 'ovner') {
            return [
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'root') { //показываем всё
            return [];
        }
        return [ //по-умолчанию показываем только себя
            'city' => $UserPermission->getCityId(),
            'group' => $UserPermission->getGroupId(),
            'manager' => $UserPermission->getId(),
        ];
    }
    
    /**
     * @return \IUserPermission
     */
    public function getUserPermission()
    {
        return $this->UserPermission;
    }
    
    
    public function getRolePrioritet()
    {
        return $this->UserPermission->getRolePrioritet();
    }
    
    protected function setUserPermission($UserPermission)
    {
        $this->UserPermission = $UserPermission;
    }

    protected function getFilter()
    {
        return $this->filter;
    }

    protected  function setFilter($filter)
    {
        $this->filter = $filter;
    }
}