<?php

namespace app\modules\analitica\models\permission;

/**
 * Для отчетов, где необходимо показывать пользователю только его данные
 * Менеджер видит себя. РГП - свою группу, Директора - всех.
 */
class ReportPermission extends GainPermission
{
    public function getPermissionByUser()
    {
        $UserPermission = $this->getUserPermission();
        $role = $UserPermission->getRole();
        if ($role == 'manager') { //показываем только себя
            return [
                'city' => $UserPermission->getCityId(),
                'group' => $UserPermission->getGroupId(),
                'manager' => $UserPermission->getId(),
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'rgp') { //показываем только свою группу
            return [
                'city' => $UserPermission->getCityId(),
                'group' => $UserPermission->getGroupId(),
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'director' || $role == 'ovner') {
            return [
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'root') { //показываем всё
            return [];
        }
        return [ //по-умолчанию показываем только себя
                'city' => $UserPermission->getCityId(),
                'group' => $UserPermission->getGroupId(),
                'manager' => $UserPermission->getId(),
        ];
    }
    
    public function visibleElements()
    {
        $UserPermission = $this->getUserPermission();
        $role = $UserPermission->getRole();
        if ($role == 'manager') { //показываем только себя
            return [];
        }
        if ($role == 'rgp') { //показываем только свою группу
            return ['manager'];
        }
        if ($role == 'director' || $role == 'root' || $role == 'ovner') { //показываем всё
            return ['city', 'group', 'manager'];
        }
        return [];
    }

}