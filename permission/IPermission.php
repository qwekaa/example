<?php

namespace app\modules\analitica\models\permission;

/**
 * Интерфейс для реализация разрешений пришедших из запроса и 
 * соединенных с ограничениями роли пользователя
 */
interface IPermission
{    
    /**
     * Получение экземляра формы с заполненными разрешениями
     * @return \app\modules\analitica\models\report\FilterForm
     */
    public function getFilterForm();
    
    /**
     * Разрешения с прмененными ограничениями в виде массива
     */
    public function getFilterWithPermission();
    
    /**
     * Получение ограничений для пользоватея
     */
    public function getPermissionByUser();
}