<?php

namespace app\modules\analitica\models\permission;

class EveningPermission extends GainPermission
{
    protected function setFilter($filter)
    {
        if (empty($filter)) {
            $filter = [
                'date_from' => (new \DateTime())->format('Y-m-d'),
                'date_to' => (new \DateTime())->add((new \DateInterval('P1D')))->format('Y-m-d'),
            ];
        }
        parent::setFilter($filter);
    }
    
    public function getPermissionByUser()
    {
        $UserPermission = $this->getUserPermission();
        $role = $UserPermission->getRole();
        if ($role == 'manager') { //показываем только себя
            return [
                'city' => [$UserPermission->getCityId()],
                'group' => [$UserPermission->getGroupId()],
                'manager' => [$UserPermission->getId()],
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'rgp') { //показываем только свою группу
            return [
                'city' => [$UserPermission->getCityId()],
                'group' => [$UserPermission->getGroupId()],
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'director' || $role == 'ovner') {
            return [
                'division' => $UserPermission->getDivisionIds(),
            ];
        }
        if ($role == 'root') { //показываем всё
            return [];
        }
        return [ //по-умолчанию показываем только себя
                'city' => [$UserPermission->getCityId()],
                'group' => [$UserPermission->getGroupId()],
                'manager' => [$UserPermission->getId()],
        ];
    }

}