<?php

namespace App\Model;

use Query\AbstractSimpleMapper;

abstract class AbstractSimpleAR
{

    protected $field_id = 'id';

    protected static $mapper = [];

    public static function model()
    {
        return new static();
    }

    abstract protected function getMapperName();
    
    /**
     * @return AbstractSimpleMapper
     */
    public function getMapper()
    {
        if (!isset(static::$mapper[static::class])) {
            $mapperClassName = $this->getMapperName();
            static::$mapper[static::class] = new $mapperClassName(static::class);
        }
        return static::$mapper[static::class];
    }

    /**
     * @return string
     */
    public function getFieldId()
    {
        return $this->field_id;
    }
    
    public function save()
    {
        return $this->getMapper()->save($this);
    }
    
    public function findOne($id)
    {
        return $this->getMapper()->find($id, $this->getFieldId());
    }
    
    public function delete()
    {
        return $this->getMapper()->delete($this->{$this->getFieldId()}, $this->field_id);
    }

    /**
     * Получить все публичные свойства со значениями, как массив,
     * где ключ = имя свойства
     * @return array
     * @throws \ReflectionException
     */
    public function asArray()
    {
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);
        $res = [];
        foreach ($properties as $prop) {
            if (!in_array($prop->getName(), $this->excludeProp())) {
                $value = $this->{$prop->getName()};
                if (is_bool($value)) {
                    $value = $value ? 1 : 0;
                }
                $res[$prop->getName()] = $value;
            }
        }
        unset($res[$this->getFieldId()]);
        return $res;
    }

    protected function excludeProp()
    {
        return [];
    }
}
