<?php

namespace Query;

use App\Model\AbstractSimpleAR;

abstract class AbstractSimpleMapper
{
    
    /**
     * @var \PHPixie\DB
     */
    protected $db;
    
    protected $className;

    public function __construct($className = null)
    {
        $this->db = \Helper\Services::db();
        $this->className = $className;
    }
    
    abstract protected function getTableName();

    /**
     * Вставить запись простой модели ActiveRecord
     * @param AbstractSimpleAR $ModelAR
     * @return integer
     * @throws \ReflectionException
     */
    public function create(AbstractSimpleAR $ModelAR)
    {
        $this->db->query('insert')->table($this->getTableName())
                ->data($ModelAR->asArray())
                ->execute();
        $ModelAR->{$ModelAR->getFieldId()} = $this->db->insert_id();
        return $ModelAR->{$ModelAR->getFieldId()};
    }

    /**
     * Обновить запись простой модели ActiveRecord
     * @param AbstractSimpleAR $ModelAR
     * @return type
     * @throws \ReflectionException
     */
    public function update(AbstractSimpleAR $ModelAR)
    {
        $query = $this->db->query('update')->table($this->getTableName())
                ->where($ModelAR->getFieldId(), $ModelAR->{$ModelAR->getFieldId()})
                ->data($ModelAR->asArray());
        return $query->execute();
    }

    /**
     * Сохранить запись. Если id нет, то новая запись, значить вставить,
     * иначе обновить
     *
     * @param AbstractSimpleAR $ModelAR
     * @return type
     * @throws \ReflectionException
     */
    public function save(AbstractSimpleAR $ModelAR)
    {
        if (is_null($ModelAR->{$ModelAR->getFieldId()})) {
            return $this->create($ModelAR);
        } else {
            return $this->update($ModelAR);
        }
    }

    /**
     * Удалеие записи по id
     *
     * @param int $id
     * @param string $field_id
     * @return type
     */
    public function delete($id, $field_id = 'id')
    {
        return $this->db->query('delete')->table($this->getTableName())
                ->where($field_id, $id)
                ->execute();
    }

    /**
     * Получить запись из БД по id
     *
     * @param integer $id
     * @param string $field_id
     * @return ContactSupportObj
     * @throws \Exception
     */
    public function find($id, $field_id = 'id')
    {
        $result = $this->db->query('select')->table($this->getTableName())
                ->where($field_id, $id)
                ->execute()
                ->single()
                ->as_object($this->className);
        if (!is_null($result)) {
            return $result;
        }
        throw new \Exception('The record has not found. Id = '.(int)$id);
    }
    
    /**
     * Получить все записи из БД
     * 
     * @return array
     */
    public function findAll()
    {
        $query = $this->db->query('select')->table($this->getTableName());
        return $query->execute()
                ->as_object($this->className);
    }
}
