<?php

namespace app\modules\analitica\models\mark;

/**
 * На расторжении
 */
class TerminateMarkQuery extends AbstractMarkQuery
{

    public function getCondition()
    {
        return "
            WHERE
                Orders.IsActive = 1
                /* AND [Orders].workflowstepid = 4 -- на расторжении */
                AND [Orders].BeginDistributionDate <= '" . $this->getDateStart() . "'
                AND [Orders].EndDistributionDateFact = '" . $this->getDateEnd() . "'
                AND [Orders].OwnerCode in ( " . $this->params['OwnerCodes']. ")
                AND [Orders].TerminationReason != 0
  	    
        ";
    }
    
    public function getQuery()
    {
        return "
        SELECT
            Orders.id as id
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and bills1.isActive = 1 and bills1.PaymentDatePlan >= '{$this->getDateStart()}' and  bills1.PaymentDatePlan <= '{$this->getDateEnd()}' ))) as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.OwnerCode
            ,Orders.Comment
            ,Orders.TerminationReason
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
        FROM [Billing].[Orders]              [Orders]
         ";
    }

}