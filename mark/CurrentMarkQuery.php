<?php

namespace app\modules\analitica\models\mark;

/**
 * Данные для текущих. Заказы на рекламе, но без счета в этом месяце
 */
class CurrentMarkQuery extends MainMarkQuery
{

    public function getCondition()
    {
        return "
        where (
            Bills.OrderId is null
            /* расторгнутые в текущем месяце тоже смотрим, т.к. если 
            перепланироваться после того, как будет расторжение, 
            то эти клиенты в планы не попадут*/
            and orders.CreatedOn < '" . $this->getDateStart() . "'
            and orders.orderType = 1
            and orders.[BeginDistributionDate] <  '" . date('Y-m-01', strtotime('+1 month', strtotime($this->getDateStart()))) . "' 
            and orders.[EndDistributionDateFact] > '".$this->getDateStart()."'
            and orders.[WorkflowStepId] in (5, 6) /* одобренные, в архиве */
            AND [Orders].OwnerCode in ( " . $this->params['OwnerCodes']. ")
        ) 
        ";
    }
    
    public function getQuery()
    {
        $dateStart = $this->getDateStart();
        $dateEnd = $this->getDateEnd();
        return "SELECT 
            Orders.id
            ,convert(money, ISNULL([Bills].PayablePlan, 0)) as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Orders.OwnerCode
        FROM [Billing].[Orders]              [Orders]
	    LEFT JOIN (SELECT * FROM Billing.Bills WHERE [Bills].[PaymentDatePlan] > '$dateStart'
            and [Bills].[PaymentDatePlan] < '$dateEnd' and Bills.IsActive = 1 ) [Bills] ON [Bills].[OrderId] = [Orders].id
        ";
    }

}