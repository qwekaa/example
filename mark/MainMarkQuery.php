<?php

namespace app\modules\analitica\models\mark;

/**
 * Этот запрос используется в нескольких типах, поэтому вынес
 */
abstract class MainMarkQuery extends AbstractMarkQuery
{
    public function getQuery()
    {
        return "SELECT 
            Orders.id
            ,convert(money, [Bills].PayablePlan) as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.OwnerCode
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Notes.text
        FROM [Billing].[Orders]         [Orders]
	    LEFT JOIN [Billing].[Bills] [Bills]  ON [Bills].[OrderId] = [Orders].id
            LEFT JOIN Shared.Notes       Notes   ON Notes.id = (select max(N.id) from Shared.Notes N where N.ParentId =[Orders].id)
        ";
    }
}
