<?php

namespace app\modules\analitica\models\mark;

use app\modules\analitica\models\work\Types;
use app\modules\analitica\models\forms\MarkOrdersForm;

class IdentyNewTypes
{
    
    /**
     * @var MarkOrdersForm
     */
    protected $MarkOrdersForm;
    
    /**
     * @var array
     */
    private $lastOrders;
    
    protected $availableSpecTask1;


    public function __construct($MarkOrdersForm)
    {
        $this->MarkOrdersForm = $MarkOrdersForm;
    }
    
    public function identy($rawData, $depth = 0)
    {
        $labelMapTypes = Types::getLabelMapGruzTypes();
        $types = [];
        if ($rawData['workflowstepid'] == 1 || $rawData['workflowstepid'] == 2) {
            $types[] = Types::NEWPROCESS;
        } elseif ($rawData['workflowstepid'] == 3){
            $types[] = Types::DECLINE;
        } else {//обойти другие типы груза и определить, что пришло
            //тариф никаких регионов! обычная отгрузка! 
            if ($this->isTariff($rawData, $depth)){
                $types[] = Types::TARIFF;
            }
            if ($this->isSpecTask1($rawData)) {
                $types[] = Types::SPECTASK1;
            }
            foreach ($labelMapTypes as $label => $gruzType) {
                if (mb_stripos($rawData['text'], $label) !== false) {
                    $types[] = $gruzType;
                }
            }
            if (empty($types)) {
                $types[] = Types::DEFAULTTYPE;
            }
        }
        return $types;
    }
    
    /**
     * Проверяет, есть ли спец.задача в заказе
     * @param array $rawData
     * @param int $depth
     * @return boolean
     */
    public function isSpecTask1($rawData)
    {
        $specTaskNames = $this->issetSpecTask1($rawData['advPositions']);
        $lastSpecTasks = $this->getSpecTask1LastMonth($rawData['firmId']);
        $this->availableSpecTask1 = array_diff($specTaskNames, $lastSpecTasks);
        return !empty($this->availableSpecTask1);
    }
    
    /**
     * Спец.задачи для переданного БЗ. Именно те, которых не было в пред.месяце
     * @return type
     */
    public function getAvailableSpecTask1()
    {
        return $this->availableSpecTask1;
    }
    
    /**
     * Есть ли в доступных (новых) позициях прайса, переданная спец.задача
     * @param string $positionName
     * @return boolean
     */
    public function hasAvailableSpecTask1($positionName)
    {
        $keywords = $this->getAvailableSpecTask1();
        foreach ($keywords as $keyword) {
            if (mb_stripos($positionName, $keyword) !== false ) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Возвращаем массив найденых спец.задач в строке по ключевым словам
     * @param string $advPositions
     * @return array
     */
    public function issetSpecTask1($advPositions)
    {
        $keywords = [
            'бренд',
            'старт в онлайн-версии (с видео)',
            'подарк',
            'ини-логотип'
        ];
        $result = [];
        foreach ($keywords as $keyword) {
            if (mb_stripos($advPositions, $keyword) !== false ) {
                $result[] = $keyword;
            }
        }
        return $result;
    }
    
    /**
     * Проверяет, если ли в примечании "тариф" и нет ли его в заказах пред.месяца
     * @param array $rawData
     * @param int $depth глубина просмотра, смотрим только пред.месяц
     * @return boolean
     */
    public function isTariff($rawData, $depth)
    {
        return mb_stripos($rawData['text'], 'тар') !== false 
                && $this->isLocalSale($rawData)
                && (
                //на первой глубине проверяем второе условие, на 
                //на второй грубине, значащие будут только предыдущие
                ++$depth > 1 || 
                !$this->whetherHaveOrdersLastMonthType($rawData['firmId'], Types::TARIFF, $depth) 
                );
    }
    
    /**
     * Проверяет, были ли тарифы в заказах в работе предыдущего месяца для переданного клиента
     * @param int $firmId Id фирмы, чтобы сопоставить заказы
     * @param int $depth глубина поиска. Берем только предыдущий месяц.
     * @return boolean
     */
    protected function whetherHaveOrdersLastMonthType($firmId, $type, $depth)
    {
        $lastOrders = $this->getLastOrders();
        //если не было в пред.месяце заказов у работы, то тарифа тоже не было
        //возвратим false (будет озанчать у переданной работы новый заказ, значит тариф)
        if (!isset($lastOrders[$firmId])) {
            return false;
        }
        $lastOrdersOnDeals = $lastOrders[$firmId];
        foreach ($lastOrdersOnDeals as $rawOrder) {
            $orderType = $this->identy($rawOrder, $depth);
            if (in_array($type, $orderType)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Получает все позиции прайса БЗ для переданного ID клиента
     * @param int $firmId Id фирмы, чтобы сопоставить заказы
     * @return array
     */
    protected function getSpecTask1LastMonth($firmId)
    {
        $lastOrders = $this->getLastOrders();
        //если не было в пред.месяце заказов у работы, то тарифа тоже не было
        //возвратим false (будет озанчать у переданной работы новый заказ, значит тариф)
        if (!isset($lastOrders[$firmId])) {
            return [];
        }
        $lastOrdersOnDeals = $lastOrders[$firmId];
        $result = [];
        foreach ($lastOrdersOnDeals as $rawOrder) {
            $positions = $this->issetSpecTask1($rawOrder['advPositions']);
            $result = array_merge($result, $positions);
        }
        return $result;
    }

    /**
     * Получает заказы пред.месяца
     * @return array
     */
    protected function getLastOrders()
    {
        if (empty($this->lastOrders)) {
            $DateStartObj = $this->MarkOrdersForm->getDateStartObj();
            $MarkOrdersForm = new MarkOrdersForm();
            $MarkOrdersForm->attributes = [
                'type' => 'new',
                'date' => $DateStartObj->sub(new \DateInterval('P1D'))->format('Y-m-01'),
                'login' => $this->MarkOrdersForm->login,
            ];
            $MarkOrdersForm->validate();
            $MarkerOrders = new NewsMarkerOrders($MarkOrdersForm, CurrentAnyQuery::class);
            $MarkQueryClass = $MarkerOrders->createMarkQueryClass();
            $this->lastOrders = \ArrayHelper::index($MarkQueryClass->getData(), null, 'firmId');
        }
        return $this->lastOrders;
    }
    
    public function isLocalSale($rawData)
    {
        if ($rawData['SourceOrganizationUnitId'] == 16 && 
                in_array($rawData['DestOrganizationUnitId'], [16, 17, 20])) {
            return true;
        }
        return false;
    }
}