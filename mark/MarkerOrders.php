<?php

namespace app\modules\analitica\models\mark;

use app\modules\analitica\models\forms\MarkOrdersForm;
use app\modules\analitica\models\work\Types;
use app\modules\analitica\models\work\Work;
use app\modules\analitica\models\work\TypeRules;

/**
 * Создает нужный экземпляр маркёра и нужный экземпляр класса для 
 * получения данных из ERM, и выполняет обход полученных данных и разметку
 */
class MarkerOrders
{
    protected $markQueryClass;
    
    protected $type;
    
    /**
     * @var MarkOrdersForm
     */
    protected $MarkOrdersForm;
    
    /**
     * @var TypeRules 
     */
    protected $TypeRules;

    /**
     * Создаем на основе типа класс маркера и выбираем, какой будем использовать
     * класс запросов для получения данных
     * @param MarkOrdersForm $MarkOrdersForm
     * @return MarkerOrders
     * @throws Exception
     */
    public static function getInstance(MarkOrdersForm $MarkOrdersForm)
    {
        $mapMarker = [
            Types::NEWS => NewsMarkerOrders::class, //у заказов свой маркёр, 
        ];
        $mapMarkQuery = [//мапинг типов к запросам на получение данных
            Types::NEWS => NewMarkQuery::class,
            Types::TERMINATE => TerminateMarkQuery::class,
            Types::CLOSEREFUSAL => CloserefusalMarkQuery::class,
            Types::CURRENT => CurrentMarkQuery::class,
            Types::RECEIPT => ReceiptMarkQuery::class,
            Types::CONTINUATION => ContinuationMarkQuery::class,
            Types::DEBTOR => DebtorMarkQuery::class,
        ];
        $type_id = $MarkOrdersForm->getTypeId();
        if (isset($mapMarker[$type_id])) {
            $markerClass = $mapMarker[$type_id];
        } else {
            $markerClass = MarkerOrders::class;
        }
        if (isset($mapMarkQuery[$type_id])) {
            return new $markerClass($MarkOrdersForm, $mapMarkQuery[$type_id]);
        }
        throw new Exception('The type has not found');
    }

    public function __construct(MarkOrdersForm $MarkOrdersForm, $markQueryClass)
    {
        $this->MarkOrdersForm = $MarkOrdersForm;
        $this->type = $MarkOrdersForm->getTypeId();
        $this->markQueryClass = $markQueryClass;
        $this->TypeRules = new TypeRules();
    }
    
    /**
     * Получение данных и обход
     */
    public function mark()
    {
        $MarkOrders = $this->createMarkQueryClass();
        $Users = $this->MarkOrdersForm->getUsers();
        //print_r($MarkOrders->getSql()); exit;
        echo 'Retrive data from ERM';
        $ordersFromErm = $MarkOrders->getData();
        echo "(" . count($ordersFromErm) . ")" . PHP_EOL;
        $ordersFromErmIndexed = \ArrayHelper::index($ordersFromErm, null, 'OwnerCode');

        foreach ($ordersFromErmIndexed as $OwnerCode => $Orders) {
            if (!isset($Users[$OwnerCode])) {
                $login = 'Неизвестно';
            } else {
                $login = $Users[$OwnerCode]->login;
            }
            echo $login.'('.count($Orders).') ';
            foreach ($Orders as $Order) {
                $this->doMark($Order);
                echo '+';
            }
            echo PHP_EOL;
        }
    }
    
    /**
     * Маркировка по типу
     * @param array $Order
     */
    protected function doMark($Order)
    {
        $Work = Work::getInstance($Order['id'], $this->type, $this->MarkOrdersForm->getDateRecord());
        $Work->setCargoIfNotChanged($Order['gruz']);
        $Work->setToPayIfNotChanged($Order['to_pay']);
        $Work->OwnerCode = $Order['OwnerCode'];
        $Work->isActive = true;
        $Work->save();
        $this->deactivateRequiredTypes($Work);
    }
    
    protected function deactivateRequiredTypes(Work $Work)
    {
        if ($Work->isActive != true) {
            return;
        }
        $TypeRules = $this->TypeRules;
        //есть ли соответствие для текущего типа в таблице деактивации
        if (!$TypeRules->hasRule($Work->type_id)) {
            return false;
        }
        $otherWorks = $Work->works;
        //Если нет других типов этого БЗ, то выходм
        if (empty($otherWorks)) {
            return;
        }
        //проиндексируем work по типам
        $otherWorks = \ArrayHelper::index($otherWorks, 'type_id');
        //типы для деактивации, согласно переданному типу
        $availableDeactivateTypes = $TypeRules->getDeactivateTypes($Work->type_id);
        //выберем только те типы, которые есть и их нужно деактивировать
        $deactivateTypes = array_intersect($availableDeactivateTypes, array_keys($otherWorks));
        foreach ($deactivateTypes as $deactivateType) {
            $otherWork = $otherWorks[$deactivateType];
            $otherWork->deactivate();
        }
        
    }
    
    /**
     * @return AbstractMarkQuery
     */
    public function createMarkQueryClass()
    {
        return new $this->markQueryClass([
            'OwnerCodes' => $this->MarkOrdersForm->getOwnerCodesAsString(),
            'Period' => $this->MarkOrdersForm,
        ]);
    }
}