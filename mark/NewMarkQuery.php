<?php

namespace app\modules\analitica\models\mark;

/**
 * Новые заказы. Включая все распределения по комментарию к заказу
 */
class NewMarkQuery extends AbstractMarkQuery
{

    public function getQuery()
    {
        return "SELECT t.*, Notes.text FROM (
        SELECT
            Orders.id
            ,convert(money, SUM([Bills].PayablePlan)) as to_pay
            ,DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1 monthDistributed
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.OwnerCode
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Orders.FirmId as firmId
            ,(
SELECT Positions.Name, OrderPositionsFinancialData.PayablePlan
  FROM [ErmRUCopy].[Billing].[PricePositions] PricePositions
  JOIN [Billing].[Prices] Prices ON Prices.Id = PricePositions.PriceId
  JOIN BIlling.Positions Positions ON Positions.id = PricePositions.PositionId
  JOIN Billing.OrderPositions OrderPositions ON OrderPositions.PricePositionId = PricePositions.Id
  JOIN Obsolete.OrderPositionsFinancialData OrderPositionsFinancialData ON OrderPositionsFinancialData.OrderPositionId = OrderPositions.id
  
  where 
   OrderPositions.IsActive = 1
  AND OrderPositions.IsDeleted = 0
  AND OrderPositions.OrderId = Orders.Id
              FOR XML PATH ('row'), ROOT('rows')) as advPositions
        FROM [Billing].[Orders]         [Orders]
        /*JOIN Billing.Deals Deals ON Deals.Id = Orders.DealId
        JOIN Billing.Clients Clients ON Clients.Id = Deals.ClientId*/
            LEFT JOIN [Billing].[Bills] [Bills]  ON [Bills].[OrderId] = [Orders].id
        WHERE
            Bills.isactive = 1
            {$this->subCondition()}
            AND Orders.OwnerCode in ( " . $this->params['OwnerCodes']. ")

        GROUP BY Orders.id, Orders.OwnerCode, Orders.FirmId, Orders.workflowstepid 
            ,Orders.[SourceOrganizationUnitId], Orders.[DestOrganizationUnitId]
            ,Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) 
        as t
        LEFT JOIN Shared.Notes Notes ON 
        Notes.id = (select max(N.id) from Shared.Notes N where N.ParentId =t.id);";
    }
    
    protected function subCondition()
    {
        return "
            AND [Orders].workflowstepid in (1, 2, 5, 6)
            AND [Orders].BeginDistributionDate = '" . date('Y-m-01', strtotime('+1 month', strtotime($this->getDateStart()))) . "'
            AND Bills.PaymentDatePlan >= '" . $this->getDateStart() . "'
            AND Bills.PaymentDatePlan <= '" . $this->getDateEnd() . "'
        ";
    }


    public function getCondition()
    {
        return "";
    }

}