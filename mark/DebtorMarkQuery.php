<?php

namespace app\modules\analitica\models\mark;

\Yii::import('application.modules.analitica.models.Oplaty');

class DebtorMarkQuery extends MainMarkQuery
{
    
    private $receipts;


    public function __construct(array $params = array())
    {
        parent::__construct($params);
        $this->receipts = $this->findOrderIdsFromDebotorsReceipts();
        
    }
    
    public function getCondition()
    {
        return '';
    }
    
    public function getQuery()
    {
        if (empty($this->receipts)) {
            return '';
        }
        $values = implode(', ', array_map(function ($receipt) {
            return '('.$receipt['order_id'].','.$receipt['price'].')';
        }, $this->receipts));
        return "SELECT 
            Orders.id
            ,receipts.price as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.OwnerCode
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Notes.text
        FROM [Billing].[Orders] [Orders]
            JOIN (VALUES $values) as receipts(order_id, price) ON receipts.order_id = Orders.Id
            LEFT JOIN Shared.Notes Notes ON Notes.id = (select max(N.id) from Shared.Notes N where N.ParentId =[Orders].id)
        ";
    }
    
    protected function findOrderIdsFromDebotorsReceipts()
    {
        $criteria = new \CDbCriteria();
        $criteria->with = ['user'];
        $criteria->addCondition('time_format >= :dateStart');
        $criteria->addCondition('time_format <= :dateEnd');
        $criteria->addCondition('dateclose < :dateStart');
        $criteria->addCondition('order_id is not null');
        $criteria->addCondition('user.OwnerCode IN ('.$this->params['OwnerCodes'].')');
        $criteria->group = 'order_id';
        $criteria->select = 'order_id, sum(price) as price';
        $criteria->params = [
            ':dateStart' => $this->getDateStart(),
            ':dateEnd' => $this->getDateEnd(),
        ];
        return \Oplaty::model()->findAll($criteria);
    }


}