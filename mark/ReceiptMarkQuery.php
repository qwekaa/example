<?php

namespace app\modules\analitica\models\mark;

/**
 * Поступления. Т.е. БЗ на рекламе со счетом в этом месяце
 */
class ReceiptMarkQuery extends MainMarkQuery
{

    public function getCondition()
    {
        return "
        where (
            [Bills].isActive = 1
            /* расторгнутые в текущем месяце тоже смотрим, т.к. если 
            перепланироваться после того, как будет расторжение, 
            то эти клиенты в планы не попадут */
            and orders.CreatedOn < '" . $this->getDateStart() . "'
            and orders.orderType = 1 /* Бартаер! */
            and orders.[BeginDistributionDate] <  '" . date('Y-m-01', strtotime('+1 month', strtotime($this->getDateStart()))) . "' /* недолжно быть оплат в след месяце */
            and orders.[EndDistributionDateFact] > '".$this->getDateStart()."'
            and orders.[WorkflowStepId] in (4, 5, 6) /* на расторжении, одобренные, в архиве */
            and [Bills].[PaymentDatePlan] > '" . $this->getDateStart() . "'
            and [Bills].[PaymentDatePlan] < '" . $this->getDateEnd() . "'
            AND [Orders].OwnerCode in ( " . $this->params['OwnerCodes']. ")
        ) ";
    }

}