<?php

namespace app\modules\analitica\models\mark;

/**
 * Закрытые отказом
 */
class CloserefusalMarkQuery extends AbstractMarkQuery
{
    
    public function getCondition()
    {
        return "
 
            WHERE
                [Orders].CreatedOn >= '" . $this->getDateStart() . "'
                AND [Orders].OwnerCode in ( " . $this->params['OwnerCodes']. ")
                AND Orders.IsActive = 0
                AND Orders.TerminationReason = 0
                AND Orders.Comment IS NOT NULL
                AND Orders.WorkflowStepId in (1,2)
";
    }

    public function getQuery()
    {
        return "
        SELECT
            Orders.Id as id
            ,convert(money,(select top 1 [Bills].PayablePlan from Billing.Bills where Orders.Id = Bills.OrderId order by Bills.PaymentDatePlan)) as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 0 and bills1.IsDeleted = 0)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1)) as gruz
            ,Orders.OwnerCode
            ,Orders.Comment
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
        FROM [Billing].[Orders] [Orders]
         ";
    }

}