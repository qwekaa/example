<?php

namespace app\modules\analitica\models\mark;

class DebtorOneMarkQuery extends MainMarkQuery
{
    
    public function getCondition()
    {
        return "
            WHERE
                Orders.Id = {$this->Period->order_id}
        ";
    }
    
    public function getQuery()
    {
        return "SELECT 
            Orders.id
            -- ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (select count(*) from [Billing].[Bills] as bills2 WHERE Orders.id = bills2.OrderId and bills2.isActive = 1)) as to_pay_to_count
            ,0 as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.OwnerCode
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Notes.text
        FROM [Billing].[Orders]         [Orders]
            LEFT JOIN Shared.Notes       Notes   ON Notes.id = (select max(N.id) from Shared.Notes N where N.ParentId =[Orders].id)
        ";
    }


}