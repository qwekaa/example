<?php

namespace app\modules\analitica\models\mark;

/**
 * Продления план
 */
class ContinuationMarkQuery extends AbstractMarkQuery
{

    public function getCondition()
    {
        return "
        where
            orders.IsActive = 1
            and DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) = DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDateFact)
            and orders.[EndDistributionDatePlan]  = '" . $this->getDateEnd() . "'
            AND [Orders].OwnerCode in ( " . $this->params['OwnerCodes']. ")
        ";
    }
    
    public function getQuery()
    {
        $dateStart = $this->getDateStart();
        $dateEnd = $this->getDateEnd();
        return "SELECT 
            Orders.id
            ,convert(money,(select top 1 [Bills].PayablePlan from Billing.Bills where Orders.Id = Bills.OrderId AND Bills.isActive = 1 order by Bills.PaymentDatePlan)) as to_pay
            ,convert(money,((select SUM(bills1.[PayablePlan]) from [Billing].[Bills] as bills1 WHERE Orders.id = bills1.OrderId and  bills1.isActive = 1)) / (DATEDIFF(month, Orders.BeginDistributionDate, Orders.EndDistributionDatePlan) + 1) ) as gruz
            ,Orders.workflowstepid
            ,Orders.[SourceOrganizationUnitId]
            ,Orders.[DestOrganizationUnitId]
            ,Orders.OwnerCode
            ,Orders.Comment
            ,Orders.TerminationReason
        FROM [Billing].[Orders] [Orders]
        ";
    }

}