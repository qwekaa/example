<?php

namespace app\modules\analitica\models\mark;

use app\modules\analitica\models\queries\Query;

/**
 * Класс для доступа к данным заказов, по типам. В дочерних классах
 * должен определятся сам запрос и условие по выборке по типам
 */
abstract class AbstractMarkQuery extends Query
{
    /**
     *
     * @var \app\modules\analitica\models\forms\MarkOrdersForm
     */
    protected $Period;

    public function __construct(array $params = array())
    {
        parent::__construct($params);
        $this->Period = $params['Period'];
        $this->db = \Yii::app()->erm;
    }
    
    abstract public function getQuery();
    
    abstract public function getCondition();
    
    public function getSql()
    {
        return $this->getQuery().' '.$this->getCondition();
    }
    
    public function execute()
    {
        $query = $this->getSql();
        if (empty($query)) {
            return [];
        }
        $this->data = $this->db->createCommand($query)
                ->queryAll();
    }
    
    protected function getDateStart()
    {
        return $this->Period->getDateStart();
    }
    
    protected function getDateEnd()
    {
        return $this->Period->getDateEnd();
    }
}