<?php

namespace app\modules\analitica\models\mark;

/**
 * Строит запрос для поиска всех БЗ на основе запроса для новых.
 */
class CurrentAnyQuery extends NewMarkQuery
{   
    protected function subCondition()
    {
        //Заказы могут быть выйти со след. месяца, их нужно учесть
        $dateStart = date('Y-m-01 00:00:00', strtotime('+1 day', strtotime($this->getDateEnd())));
        return "
            AND [Orders].workflowstepid in (5, 6)
            AND [Orders].BeginDistributionDate <= '" . $dateStart . "'
            AND [Orders].EndDistributionDateFact >= '" . $this->getDateEnd() . "'
        ";
    }
}