<?php

namespace app\modules\analitica\models\mark;

use app\modules\analitica\models\work\Types;
use app\modules\analitica\models\work\Work;
use app\modules\analitica\models\forms\MarkOrdersForm;

/**
 * Маркёр для новых, т.к. ещё идет распределение по комментарию к заказу
 */
class NewsMarkerOrders extends MarkerOrders
{
    /**
     * Класс определяет тип заказа
     * @var InspectorNewTypes
     */
    protected $IdentyNewTypes;
    
    public function __construct(MarkOrdersForm $MarkOrdersForm, $markQueryClass)
    {
        parent::__construct($MarkOrdersForm, $markQueryClass);
        $this->IdentyNewTypes = new IdentyNewTypes($this->MarkOrdersForm);
    }


    protected function doMark($Order)
    {
        $orderTypes = $this->IdentyNewTypes->identy($Order);
        $Works = Work::getWorksOnOrderId($Order['id'], $this->MarkOrdersForm->getDateRecord());
        //если нет записи др.типов, будем добавлять все
        if (empty($Works)) {
            $insert = $orderTypes;
        } else {
            //если есть др.типы, то определим, какие типы пришли, и что обновить, удалить, вставить
            //здесь основной упор делается на перемещение между подтипами новых БЗ
            // новые, тариф, спец.задача, продление и т.д.
            $currTypes = \ArrayHelper::getColumn($Works, 'type_id');
            $update = array_intersect($currTypes, $orderTypes);
            $delete = array_diff($currTypes, $orderTypes);
            $insert = array_diff($orderTypes, $currTypes);
            $this->process($Order, $update, true);
            $this->process($Order, $delete, false);
        }
        $this->process($Order, $insert, true);
    }
    
    protected function process($Order, $type_ids, $isActive)
    {
        foreach ($type_ids as $type) {
            $Work = Work::getInstance($Order['id'], $type, $this->MarkOrdersForm->getDateRecord());
            $Work->setCargoIfNotChanged($this->getCargoDependType($Order, $type));
            $Work->setToPayIfNotChanged($Order['to_pay']);
            $Work->OwnerCode = $Order['OwnerCode'];
            $Work->isActive = $isActive;
            $Work->save();
            $this->deactivateRequiredTypes($Work);//декативация по типам-статусам
        }
    }
    
    protected function getCargoDependType($Order, $type_id)
    {
        //Для всех типов берем груз БЗ
        if ($type_id != Types::SPECTASK1) {
            return $Order['gruz'];
        }
        //Для Спец.задач 1 берем груз от рекламной позиции
        return $this->getCargoFromSpecTast1($Order);
    }
    
    protected function getCargoFromSpecTast1($Order)
    {
        $sum = 0;
        $AdvPositions = new \SimpleXMLElement($Order['advPositions']);
        foreach ($AdvPositions as $Position) {
            if (!empty($this->IdentyNewTypes->hasAvailableSpecTask1($Position->Name))) {
                $sum+= (float)$Position->PayablePlan;
            }
        }
        return $sum / (!empty($Order['monthDistributed'])?$Order['monthDistributed']:1);
    }
}
